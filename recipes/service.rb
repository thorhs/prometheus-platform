#
# Copyright (c) 2016-2017 Sam4Mobile, 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

auto_restart = node[cookbook_name]['auto_restart']

node[cookbook_name]['components'].each_pair do |comp, config|
  next unless config['install?']
  configfile = "#{node[cookbook_name]['prefix_home']}/#{comp}/#{comp}.yml"

  if node['platform_family'] == 'rhel' && node['platform_version'].to_i == 7
    systemd_unit "#{comp}.service" do
      content config['unit']
      action %i[create enable start]
      subscribes :reload_or_try_restart, "file[#{configfile}]" if auto_restart
      subscribes :try_restart, "systemd_unit[#{comp}.service]" if auto_restart
    end
  else
    template "/etc/init.d/#{comp}" do
      source 'init_script.erb'
      mode '0744'
      owner 'root'
      group 'root'
      variables(
        component: comp
      )
    end

    template "/etc/sysconfig/#{comp}" do
      source 'config.erb'
      mode '0744'
      owner 'root'
      group 'root'
      variables(
        args: config['unit']['cli']
      )
    end

    service comp.to_s do
      action [:enable, :start]
      subscribes :restart, "file[#{configfile}]" if auto_restart
      subscribes :restart, "file[/etc/sysconfig/#{comp}]" if auto_restart
      subscribes :restart, "file[/etc/init.d/#{comp}]" if auto_restart
    end
  end
end
